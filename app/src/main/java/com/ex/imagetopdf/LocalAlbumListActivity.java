package com.ex.imagetopdf;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.ImageDownloader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 获取相册列表数据
 */
public class LocalAlbumListActivity extends Activity implements AdapterView.OnItemClickListener {
    private ListView listView;
    private Map<String, List<LocalFile>> folders = new HashMap<>();
    List<String> foldersName = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_local_album_list);
        listView = findViewById(R.id.listview);
        folders = Util.getInstance().getFolders(this);
        listView.setAdapter(new BaseAdapter() {

            @Override
            public int getCount() {
                return folders.size();
            }

            @Override
            public Object getItem(int i) {
                return i;
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                ViewHolder viewHolder;
                List<LocalFile> localFiles;

                if (view == null) {
                    viewHolder = new ViewHolder();
                    view = LayoutInflater.from(LocalAlbumListActivity.this).inflate(R.layout.album_list_item, null);
                    viewHolder.iv_show = view.findViewById(R.id.iv_album_list);
                    viewHolder.iv_back = view.findViewById(R.id.iv_right_arrow);
                    viewHolder.tv = view.findViewById(R.id.tv_album_list);
                    view.setTag(viewHolder);
                    for (Map.Entry entry : folders.entrySet()) {
                        String key = (String) entry.getKey();
                        foldersName.add(key);
                    }
                    localFiles = folders.get(foldersName.get(i));
                    if (foldersName != null && localFiles != null) {
                        int total = localFiles.size();
                        viewHolder.tv.setText(foldersName.get(i) + "(" + String.valueOf(total) + ")");
                        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                                .threadPriority(Thread.NORM_PRIORITY - 2)
                                .denyCacheImageMultipleSizesInMemory()
                                .build();
                        if (!ImageLoader.getInstance().isInited()) {
                            ImageLoader.getInstance().init(config);
                        }
                        DisplayImageOptions options = new DisplayImageOptions.Builder()
                                .showStubImage(R.mipmap.ic_launcher)  //加载之前的图片
                                .showImageForEmptyUri(R.mipmap.ic_launcher) //图片为空的时候
                                .showImageOnFail(R.mipmap.ic_launcher) //加载失败的图片
                                .cacheInMemory()
                                .cacheOnDisc()
                                .displayer(new RoundedBitmapDisplayer(5))
                                .build();
                        ImageLoader.getInstance().displayImage(ImageDownloader.Scheme.FILE.wrap(localFiles.get(0).getPhotoname()), viewHolder.iv_show, options);
                    }
                } else {
                    viewHolder = (ViewHolder) view.getTag();
                }
                return view;
            }

            class ViewHolder {
                ImageView iv_show, iv_back;
                TextView tv;

            }
        });
        listView.setOnItemClickListener(this);
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent(this, LocalAlbumActivity.class);
        intent.putExtra("key", foldersName.get(i));
        startActivity(intent);
    }
}
