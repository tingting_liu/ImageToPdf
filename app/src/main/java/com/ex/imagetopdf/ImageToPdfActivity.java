package com.ex.imagetopdf;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.pdf.PdfWriter;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.download.ImageDownloader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ImageToPdfActivity extends Activity implements View.OnClickListener {
    private EditText et_pdf_name;
    private Button btn_pdf_gen, btn_pdf_cancel;
    private List<String> states = new ArrayList<>();
    private GridView gridView;
    private GridViewAdapter adapter;
    private String pdfName;
    private String PdfDir = Environment.getExternalStorageDirectory() + File.separator + "A_PDFFile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_to_pdf);
        et_pdf_name = findViewById(R.id.et_pdf_name);
        btn_pdf_gen = findViewById(R.id.btn_pdf_gen);
        btn_pdf_cancel = findViewById(R.id.btn_pdf_cancel);
        gridView = findViewById(R.id.gridview);
        btn_pdf_cancel.setOnClickListener(this);
        btn_pdf_gen.setOnClickListener(this);
        states = getIntent().getStringArrayListExtra("states");
        if (states != null && states.size() > 0) {
            adapter = new GridViewAdapter(this, states);
            gridView.setAdapter(adapter);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_pdf_gen:
                if (TextUtils.isEmpty(et_pdf_name.getText().toString())) {
                    pdfName = System.currentTimeMillis() + ".pdf";
                } else {
                    pdfName = et_pdf_name.getText().toString() + ".pdf";
                }
                imageToPdf(states, pdfName);
                break;
            case R.id.btn_pdf_cancel:
                finish();
                break;
        }
    }

    /**
     * 图片转换为pdf文件
     *
     * @param paths
     * @param pdfName
     */
    private void imageToPdf(List<String> paths, String pdfName) {
        if (paths == null || paths.size() == 0) {
            return;
        }
        boolean isOk = true;
        Image imageJpg;
        String fPath = null;
        File fFile = null;
        File file = new File(PdfDir);
        if (!file.exists()) {
            file.mkdirs();
        }
        try {
            fFile = new File(PdfDir, pdfName);
            fPath = fFile.getAbsolutePath();
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(fPath));
            document.open();
            for (int i = 0; i < paths.size(); i++) {
                String path = paths.get(i);
                File file1 = new File(path);
                if (file1.exists()) {
                    String nPath = Util.getInstance().getFilePath(this, path);
                    imageJpg = Image.getInstance(nPath);
                    float height = imageJpg.getHeight();
                    float width = imageJpg.getWidth();
                    int percent = getPercent2(height, width);
                    imageJpg.setAlignment(Image.MIDDLE);
                    imageJpg.scalePercent(percent);
                    document.add(imageJpg);
                    File file2 = new File(nPath);
                    if (file2.exists()) {
                        file2.delete();
                    }
                }
            }
            document.close();
        } catch (DocumentException e) {
            e.printStackTrace();
            isOk = false;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            isOk = false;
        } catch (IOException e) {
            e.printStackTrace();
            isOk = false;
        }
        String msg;
        if (isOk) {
            msg = "pdf创建成功!\n" + fPath;
        } else {
            msg = "pdf创建失败!";
        }
        final File finalFPath = fFile;
        new AlertDialog.Builder(this)
                .setTitle("提示")
                .setMessage(msg)
                .setPositiveButton("查看", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        openPdf(finalFPath);
                    }
                }).setNegativeButton("取消", null)
                .show();
    }

    /**
     * 打开pdf文件预览
     * Uri uri = Uri.fromFile(file);
     * android7.0 uri暴露异常，所以要使用FileProvider,个人感觉很麻烦
     * android 提供了另外一种强制模式，感觉很方便，直接使用uri.fromFile(file)
     * 点击查看时如果不跳转，说明你没有第三方的阅读器，可以下载福昕PDF阅读器来解析
     * @param file
     */
    private void openPdf(File file) {
        Uri uri = null;
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
//            StrictMode.setVmPolicy(builder.build());
//            uri = Uri.fromFile(file);
            uri = FileProvider.getUriForFile(this, "com.ex.imagetopdf.FileProvider", file);
        } else {
            uri = Uri.fromFile(file);
        }
        intent.setDataAndType(uri, "application/pdf");
        startActivity(intent);

    }

    private synchronized int getPercent2(float height, float width) {
        int p = 0;
        float p2 = 0.0f;
        p2 = 530 / width * 100;
        p = Math.round(p2);
        return p;
    }

    private class GridViewAdapter extends BaseAdapter {
        private Context context;
        private List<String> states = new ArrayList<>();

        public GridViewAdapter(Context context, List<String> states) {
            this.context = context;
            this.states = states;
        }

        @Override
        public int getCount() {
            return states.size();
        }

        @Override
        public Object getItem(int i) {
            return states.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            if (view == null) {
                viewHolder = new ViewHolder();
                view = LayoutInflater.from(context).inflate(R.layout.photo_item2, null);
                viewHolder.iv_photo = view.findViewById(R.id.iv_photo);
                viewHolder.iv_delete = view.findViewById(R.id.iv_delete);
                final String path = (String) getItem(i);
                ImageLoader.getInstance().displayImage(ImageDownloader.Scheme.FILE.wrap(path), viewHolder.iv_photo, Util.prepareImageloador(context));
                viewHolder.iv_delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        states.remove(path);
                        notifyDataSetChanged();
                    }
                });
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();

            }
            return view;

        }

        private class ViewHolder {
            ImageView iv_photo, iv_delete;
        }
    }
}
