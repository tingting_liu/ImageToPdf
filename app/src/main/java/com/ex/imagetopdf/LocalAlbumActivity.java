package com.ex.imagetopdf;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.download.ImageDownloader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 获取相册列表详情对应的数据
 */
public class LocalAlbumActivity extends Activity implements View.OnClickListener {
    private ImageView iv_back;
    private TextView tv_cancel, album_title;
    private GridView gridView;
    private TextView tv_total, tv_finish;
    private MyAdapter adapter;
    private String key;
    private List<LocalFile> localFiles = new ArrayList<>();
    private Map<String, List<LocalFile>> folders = new HashMap<>();
    private List<String> states = new ArrayList<>();
    private List<String> selectedStates = new ArrayList<>();
    private Map<String, Boolean> maps = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_local_album);
        iv_back = findViewById(R.id.album_back);
        album_title = findViewById(R.id.album_title);
        tv_cancel = findViewById(R.id.album_cancel);
        gridView = findViewById(R.id.gridview);
        tv_total = findViewById(R.id.total);
        tv_finish = findViewById(R.id.finish);
        tv_finish.setOnClickListener(this);
        iv_back.setOnClickListener(this);
        tv_cancel.setOnClickListener(this);
        folders = Util.getInstance().getFolders(this);
        key = getIntent().getStringExtra("key");
        album_title.setText(key);
        if (!TextUtils.isEmpty(key) && folders != null) {
            localFiles = folders.get(key);
            adapter = new MyAdapter(LocalAlbumActivity.this, localFiles);
            gridView.setAdapter(adapter);
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.album_back:
                Intent intent = new Intent(this, LocalAlbumListActivity.class);
                startActivity(intent);
                break;
            case R.id.album_cancel:
                finish();
                break;
            case R.id.finish:
                Intent data = new Intent(this, ImageToPdfActivity.class);
                data.putStringArrayListExtra("states", (ArrayList<String>) selectedStates);
                startActivity(data);
                break;
        }
    }

    private class MyAdapter extends BaseAdapter implements CompoundButton.OnCheckedChangeListener {
        private Context context;
        private List<LocalFile> list = new ArrayList<>();

        private MyAdapter(Context context, List<LocalFile> list) {
            this.context = context;
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int i) {
            return list.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        /**
         * gridview上下滑动过程
         * checkbox 组件复用导致选择10个翻到下一页可能勾选了15个
         *
         * @param i
         * @param view
         * @param viewGroup
         * @return
         */
        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            if (view == null) {
                viewHolder = new ViewHolder();
                view = LayoutInflater.from(context).inflate(R.layout.photo_item, null);
                viewHolder.iv = view.findViewById(R.id.iv);
                viewHolder.checkBox_state = view.findViewById(R.id.checkbox_state);
                view.setTag(viewHolder);
                Log.i("liutingting", "getView: path-----------" + list.get(i).getPhotoname());
            } else {
                viewHolder = (ViewHolder) view.getTag();
                Log.i("liutingting2", "getView: path-----------" + list.get(i).getPhotoname());
            }
            String path = list.get(i).getPhotoname();
            viewHolder.checkBox_state.setTag(path);
            if (selectedStates != null && selectedStates.size() > 0) {
                if (selectedStates.contains(path)) {
                    viewHolder.checkBox_state.setChecked(true);
                } else {
                    viewHolder.checkBox_state.setChecked(false);
                }
            }
            ImageLoader.getInstance().displayImage(ImageDownloader.Scheme.FILE.wrap(path), viewHolder.iv, Util.prepareImageloador(context));
            viewHolder.checkBox_state.setOnCheckedChangeListener(this);
            return view;
        }

        /**
         * 图片被选中
         *
         * @param compoundButton
         * @param b
         */
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            selectedStates.addAll(states);
            String path = "";
            if (b) {
                path = (String) compoundButton.getTag();
                if (!TextUtils.isEmpty(path)) {
                    if (!selectedStates.contains(path)) {
                        selectedStates.add(path);
                    }
                }
            } else {
                path = (String) compoundButton.getTag();
                if (!TextUtils.isEmpty(path)) {
                    if (selectedStates.contains(path)) {
                        selectedStates.remove(path);
                    }
                }
            }
            tv_total.setVisibility(View.VISIBLE);
            tv_total.setText(String.valueOf(selectedStates.size()) + "/20");
        }

        private class ViewHolder {
            ImageView iv;
            CheckBox checkBox_state;
        }
    }
}
