package com.ex.imagetopdf;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 帮助获取相册列表数据
 * Created by tingting.liu on 2018/9/19.
 */

public class Util {
    private static volatile Util instance;
    private String[] QUERY_DATA = new String[]{MediaStore.Images.Media._ID, MediaStore.Images.Media.DATA};
    public String dirFile = Environment.getExternalStorageDirectory() + File.separator + "A_PDFFile" + File.separator;

    public static Util getInstance() {
        if (instance == null) {
            synchronized (Util.class) {
                if (instance == null) {
                    instance = new Util();
                }
            }
        }
        return instance;
    }

    public Map<String, List<LocalFile>> getFolders(Context context) {
        Map<String, List<LocalFile>> folders = new HashMap<>();
        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, QUERY_DATA, null, null, null, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                int id = cursor.getInt(0);
                String data = cursor.getString(1);
                LocalFile localFile = new LocalFile();
                localFile.setPhotoname(data);
                File file = new File(data);
                if (file.isDirectory()) {
                    continue;
                }
                String key = file.getParentFile().getName();
                if (folders.containsKey(key)) {
                    List<LocalFile> temp = folders.get(key);
                    temp.add(localFile);
                    folders.put(key, temp);
                } else {
                    List<LocalFile> localFiles = new ArrayList<>();
                    localFiles.add(localFile);
                    folders.put(key, localFiles);
                }
            }
        }
        return folders;
    }

    public static DisplayImageOptions prepareImageloador(Context context) {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context.getApplicationContext())
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .build();
        if (!ImageLoader.getInstance().isInited()) {
            ImageLoader.getInstance().init(config);
        }
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showStubImage(R.mipmap.ic_launcher)  //加载之前的图片
                .showImageForEmptyUri(R.mipmap.ic_launcher) //图片为空的时候
                .showImageOnFail(R.mipmap.ic_launcher) //加载失败的图片
                .cacheInMemory()
                .cacheOnDisc()
                .displayer(new RoundedBitmapDisplayer(5))
                .build();
        return options;
    }

    /**
     * 保存相册图片到新文件夹下
     *
     * @param path
     * @return
     */
    public String getFilePath(Activity context,String path) {
        if (path == null) {
            return null;
        }
        File temp = null;
        float scale = 0.0f;
        String tempPath = System.currentTimeMillis() + ".png";
        File dir = new File(dirFile);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        Display display = context.getWindow().getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);
        int w0 =outMetrics.widthPixels;
        int h0=outMetrics.heightPixels;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            int w = options.outWidth;
            int h = options.outHeight;
            if (w <= h) {
                scale = (float) w / w0;
            } else {
                scale = (float) h / h0;
            }
            options.inSampleSize = (int) scale;
            options.inJustDecodeBounds = false;
            Bitmap source = BitmapFactory.decodeFile(path, options);
            temp = new File(dir, tempPath);
            if (!temp.exists()) {
                temp.createNewFile();
            }
            FileOutputStream outputStream = new FileOutputStream(temp);
            source.compress(Bitmap.CompressFormat.PNG, 80, outputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return temp.getAbsolutePath();
    }

}
