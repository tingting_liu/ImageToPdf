package com.ex.imagetopdf;

/**
 * Created by tingting.liu on 2018/9/19.
 */

public class LocalFile {
    private String  photoname;
    private String thumbnail;

    public String getPhotoname() {
        return photoname;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setPhotoname(String photoname) {
        this.photoname = photoname;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}
